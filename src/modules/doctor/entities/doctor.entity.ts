
import { Consultation } from "src/modules/consultation/entities/consultation.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn, Timestamp } from "typeorm";

@Entity("doctors")
export class Doctor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: "name" })
  name: string;

  @Column({ name: "specialist" })
  specialist: string;

  @Column({ name: "created_at", type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  createdAt: Timestamp;

  @Column({ name: "updated_at", type: "timestamp" })
  updatedAt: Timestamp;

  @OneToMany(type => Consultation, consultation => consultation.doctor)
  consultations: Consultation[];
}
