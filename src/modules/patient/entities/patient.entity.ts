import { Consultation } from "src/modules/consultation/entities/consultation.entity";
import { PatientDisease } from "src/modules/patient-disease/entities/patient-disease.entity";
import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn, Timestamp } from "typeorm";

@Entity("patients")
export class Patient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: "name" })
  name: string;

  @Column({ name: "created_at", type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  createdAt: Timestamp;

  @Column({ name: "updated_at", type: "timestamp" })
  updatedAt: Timestamp;

  @OneToMany(() => Consultation, consultation => consultation.patient)
  consultations: Consultation[];

  @OneToMany(() => PatientDisease, patientDisease => patientDisease.patient)
  patientDiseases: PatientDisease[];
}
