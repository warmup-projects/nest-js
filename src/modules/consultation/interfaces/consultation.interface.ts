export interface Consultation {
  id: number;
  patient_id: number;
  doctor_id: number;
}