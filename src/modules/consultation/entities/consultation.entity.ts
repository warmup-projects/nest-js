import { Doctor } from "src/modules/doctor/entities/doctor.entity";
import { Patient } from "src/modules/patient/entities/patient.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, Timestamp } from "typeorm";

@Entity("consultations")
export class Consultation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: "patient_id", type: "int" })
  patientId: number;

  @Column({ name: "doctor_id", type: "int" })
  doctorId: number;

  @Column({ name: "created_at", type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  createdAt: Timestamp;

  @Column({ name: "updated_at", type: "timestamp" })
  updatedAt: Timestamp;

  @ManyToOne(() => Doctor, doctor => doctor.consultations)
  @JoinColumn({ name: "doctor_id" })
  doctor: Doctor;

  @ManyToOne(() => Patient, patient => patient.consultations)
  @JoinColumn({ name: "patient_id" })
  patient: Patient;
}
