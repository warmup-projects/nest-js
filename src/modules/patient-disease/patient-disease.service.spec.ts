import { Test, TestingModule } from '@nestjs/testing';
import { PatientDiseaseService } from './patient-disease.service';

describe('PatientDiseaseService', () => {
  let service: PatientDiseaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PatientDiseaseService],
    }).compile();

    service = module.get<PatientDiseaseService>(PatientDiseaseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
