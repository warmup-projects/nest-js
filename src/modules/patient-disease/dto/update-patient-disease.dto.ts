import { PartialType } from '@nestjs/mapped-types';
import { CreatePatientDiseaseDto } from './create-patient-disease.dto';

export class UpdatePatientDiseaseDto extends PartialType(CreatePatientDiseaseDto) {}
