import { Injectable } from '@nestjs/common';
import { CreatePatientDiseaseDto } from './dto/create-patient-disease.dto';
import { UpdatePatientDiseaseDto } from './dto/update-patient-disease.dto';

@Injectable()
export class PatientDiseaseService {
  create(createPatientDiseaseDto: CreatePatientDiseaseDto) {
    return 'This action adds a new patientDisease';
  }

  findAll() {
    return `This action returns all patientDisease`;
  }

  findOne(id: number) {
    return `This action returns a #${id} patientDisease`;
  }

  update(id: number, updatePatientDiseaseDto: UpdatePatientDiseaseDto) {
    return `This action updates a #${id} patientDisease`;
  }

  remove(id: number) {
    return `This action removes a #${id} patientDisease`;
  }
}
