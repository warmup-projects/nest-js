import { Test, TestingModule } from '@nestjs/testing';
import { PatientDiseaseController } from './patient-disease.controller';
import { PatientDiseaseService } from './patient-disease.service';

describe('PatientDiseaseController', () => {
  let controller: PatientDiseaseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PatientDiseaseController],
      providers: [PatientDiseaseService],
    }).compile();

    controller = module.get<PatientDiseaseController>(PatientDiseaseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
