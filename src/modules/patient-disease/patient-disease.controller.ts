import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PatientDiseaseService } from './patient-disease.service';
import { CreatePatientDiseaseDto } from './dto/create-patient-disease.dto';
import { UpdatePatientDiseaseDto } from './dto/update-patient-disease.dto';

@Controller('patient-disease')
export class PatientDiseaseController {
  constructor(private readonly patientDiseaseService: PatientDiseaseService) {}

  @Post()
  create(@Body() createPatientDiseaseDto: CreatePatientDiseaseDto) {
    return this.patientDiseaseService.create(createPatientDiseaseDto);
  }

  @Get()
  findAll() {
    return this.patientDiseaseService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.patientDiseaseService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePatientDiseaseDto: UpdatePatientDiseaseDto) {
    return this.patientDiseaseService.update(+id, updatePatientDiseaseDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.patientDiseaseService.remove(+id);
  }
}
