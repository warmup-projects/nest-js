import { Module } from '@nestjs/common';
import { PatientDiseaseService } from './patient-disease.service';
import { PatientDiseaseController } from './patient-disease.controller';

@Module({
  controllers: [PatientDiseaseController],
  providers: [PatientDiseaseService],
})
export class PatientDiseaseModule {}
