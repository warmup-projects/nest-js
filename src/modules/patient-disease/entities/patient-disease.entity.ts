import { Patient } from "src/modules/patient/entities/patient.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, Timestamp } from "typeorm";

@Entity("patient_diseases")
export class PatientDisease {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: "patient_id", type: "int" })
  patientId: number;

  @Column({ name: "name" })
  name: string;

  @Column({ name: "has_recovered", type: "bit", default: false })
  hasRecovered: boolean;

  @Column({ name: "created_at", type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  createdAt: Timestamp;

  @Column({ name: "updated_at", type: "timestamp" })
  updatedAt: Timestamp;

  @ManyToOne(type => Patient, patient => patient.patientDiseases)
  patient: Patient
}
