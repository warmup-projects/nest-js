import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Consultation } from 'src/modules/consultation/entities/consultation.entity';
import { Doctor } from 'src/modules/doctor/entities/doctor.entity';
import { PatientDisease } from 'src/modules/patient-disease/entities/patient-disease.entity';
import { Patient } from 'src/modules/patient/entities/patient.entity';

const typeOrmConfig = (configService: ConfigService): TypeOrmModuleOptions => ({
  type: "mssql",
  host: configService.get("DATABASE_HOST"),
  port: parseInt(configService.get("DATABASE_PORT") ?? "1433"),
  database: configService.get("DATABASE_NAME"),
  username: configService.get("DATABASE_USER"),
  password: configService.get("DATABASE_PASSWORD"),
  entities: [Consultation, Doctor, Patient, PatientDisease],
  options: {
    trustServerCertificate: true,
  },
});

export default typeOrmConfig;